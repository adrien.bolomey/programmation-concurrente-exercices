package philosophers;

import java.util.Random;

public class Philosopher extends Thread {
	
	private Chopstick first, second;
	private Random random;
	private int thinkCount;
	private int idPhilisopher = 0;

	public Philosopher(Chopstick left, Chopstick right, int idPhilisopher) {
		if(left.getId() < right.getId()) {
			first = left; second = right;
		} else {
	    	first = right; second = left;
	    }
		this.idPhilisopher = idPhilisopher;
		random = new Random();
	}

	public void run() {
		try {
			while(true) {
				++thinkCount;
				if (thinkCount % 10 == 0)
					System.out.println("Philosopher " + idPhilisopher + " - " + this + " has thought " + thinkCount + " times");
				Thread.sleep(random.nextInt(1000));     // Think for a while
				synchronized(first) {                   // Grab first chopstick
					synchronized(second) {                // Grab second chopstick
						System.out.println("Philosopher " + idPhilisopher + " is eating");
						Thread.sleep(random.nextInt(1000)); // Eat for a while
					}
				}
			}
		} catch(InterruptedException e) {
			System.out.println("Philosopher Thread Interruption Exception");
			e.printStackTrace();
		}
	}
}