package philosophers;

public class DiningPhilosophers {

	static final int PHILOSOPHERS = 5;
	
	public static void main(String[] args) throws InterruptedException {
	    
		Philosopher[] philosophers = new Philosopher[PHILOSOPHERS];
		Chopstick[] chopsticks = new Chopstick[PHILOSOPHERS];
	    
		for (int i = 0; i < PHILOSOPHERS; ++i) chopsticks[i] = new Chopstick(i);
		
	    for (int i = 0; i < PHILOSOPHERS; ++i) {
	    	philosophers[i] = new Philosopher(chopsticks[i], chopsticks[(i + 1) % PHILOSOPHERS], i);
	    	philosophers[i].start();
	    }
	    
	    for (int i = 0; i < PHILOSOPHERS; ++i) philosophers[i].join();
	}
}