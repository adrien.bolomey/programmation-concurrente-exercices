package philosophers_v2;

import java.util.Arrays;

// liste des Fourchettes que doivent se partager les philosophes
public class Fourchettes {
 
  boolean[] lesFourchettes; // tableau d'occupation des fourchettes false = occupee, true = libre
  
  int nbFourchettes;
   
  // constructeur initialisant la taille et le tableau des fourchettes a true
  public Fourchettes(int nbFourchettes) {
	  this.nbFourchettes = nbFourchettes;
	  lesFourchettes = new boolean[nbFourchettes];
	  Arrays.fill(lesFourchettes, true);
  }
   
  /** fonction appelee par un processus philosophe i.
  * Si la fourchette de gauche (i) et de droite (i+1) modulo nbr de fourchettes
  * est libre alors le philosophe les prend,
  * sinon, il est mis en attente */
  public synchronized void prendre(int noFourchette) {
    int gauche = noFourchette;
    int droite = (noFourchette + 1) % nbFourchettes;      
    // while (!lesFourchettes[gauche] || !lesFourchettes[droite]) { 
    while (!(lesFourchettes[gauche] && lesFourchettes[droite])) {	// alternative 
      try { /* Thread.currentThread(). */ wait(); } catch (InterruptedException e) {} 	// wait dans synchronized, risque de lockout !!
    }
    lesFourchettes[gauche] = false;
    lesFourchettes[droite] = false;
  }
   
  /** fonction appelee par un processus philosophe i.
  * libere la fourchette de gauche (i) et de droite (i+1) modulo nbr de fourchettes
  * et reveille les processus en attente sur les fourchettes */
  public synchronized void deposer(int noFourchette) {
    int gauche = noFourchette;
    int droite = (noFourchette + 1 ) % nbFourchettes;      
    lesFourchettes[gauche] = true;
    lesFourchettes[droite] = true;
    notifyAll(); // reveille les processus en attente de fourchettes 
  }      
}

