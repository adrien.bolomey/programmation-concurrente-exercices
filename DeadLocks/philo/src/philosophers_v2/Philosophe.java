package philosophers_v2;
import java.util.Random;

public class Philosophe extends Thread {
	  
	  int noThread;					// no du philosophe  
	  int nbBouchees;				// nb de bouchees restantes dans l'assiette
	  long debut;					// date de debut du diner
	  double tempsTotalRepas;		// temps total du repas pour le philosophe
	 
	  Fourchettes lesFourchettes;	// liste de fourchettes

	  static final int TempsBaseBouchee=100;	// temps min d'une bouchee en milliseconde
	  static final int TempsBouchee=500;		// temps max en plus pour une bouchee en milliseconde
	  static final int TempsMinPensee=100;		// temps min d'une pensee en millisecond
	  static final int TempsPensee=500;			// temps max en plus pour une pensee en milliseconde
	 
	  // constructeur par défaut
	  Philosophe() {}	
	 
	  // constructeur spécifique hors groupe de threads
	  Philosophe(int noThread, int nbBouchees, Fourchettes lesFourchettes) {
	    super("philo" + noThread);	// fixe le mon du thread philosophe	
	    this.noThread = noThread;
	    this.nbBouchees = nbBouchees;
	    this.lesFourchettes = lesFourchettes;
	  }
	 
	  // constructeur spécifique dans un groupe de threads
	  Philosophe(ThreadGroup groupe, int noThread, int nbBouchees, Fourchettes lesFourchettes) {
	    super(groupe, "philo" + noThread);	// fixe le mon du thread philosophe dans le groupe
	    this.noThread = noThread;
	    this.nbBouchees = nbBouchees;
	    this.lesFourchettes = lesFourchettes;
	  }
	 
	  /** fonction principale du philosophe : cycle sur manger, penser.
	   * Pour manger, il prend la fourchette de droite et celle de gauche.
	   * Donc i prend la fourchette i et i+1 (modulo traité dans la classe Fourchettes !!).
	   * Le philosophe garde les fourchettes un certains temps et les depose ensuite.
	   * La boucle se termine lorsque le philosophe a termine ses bouchees.
	   * */
	  public void run() {
	    debut = System.currentTimeMillis();
	    Random hasard = new Random();
	    while(nbBouchees > 0) {
	      System.out.println(this.getName() + " : je demande les fourchettes, j'attends en pensant");
	      
	      lesFourchettes.prendre(noThread);	          
	      nbBouchees--;
	      System.out.println(this.getName() + " : j'ai obtenu les fourchettes, je mange, il me reste " + nbBouchees + " bouchees.");
	      try {  
	    	  Thread.sleep(Philosophe.TempsBaseBouchee +  hasard.nextInt(Philosophe.TempsBouchee ));
	      } catch (InterruptedException e) {}
	      
	      lesFourchettes.deposer(noThread);
	      System.out.println(this.getName() + " : je pense un peu après ma bouchée...");
	      try {  
	    	  Thread.sleep(Philosophe.TempsMinPensee +  hasard.nextInt(Philosophe.TempsPensee ));
	      } catch (InterruptedException e) {}
	    }
	    long fin = System.currentTimeMillis();
	    tempsTotalRepas = (fin-debut)/1000d;
	    System.out.printf("%s : j'ai fini en %.3f s.\n", this.getName(),tempsTotalRepas);
	  }
	}

