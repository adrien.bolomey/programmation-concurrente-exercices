package philosophers_v2;

public class Diner {

	public static void main(String[] args) {
	    
		final int dim = 5;			// nbr de philosophes ... (Est-ce que ce nom de variable est judicieux ?)
		final int nbBouchees = 4; 	// nbre de bouchées par philosophe	
		
	    Fourchettes fourchettes = new Fourchettes(dim);
	    Philosophe[] mangeurs = new Philosophe[dim];
	 
	    ThreadGroup groupeOfPhilos = new ThreadGroup("philos");
	 
	    for (int i =0; i<dim; i++)
	      mangeurs[i] = new  Philosophe(groupeOfPhilos, i, nbBouchees, fourchettes);
	 
	    long dateDepart = System.currentTimeMillis();
	    for (Philosophe mangeur:mangeurs)  mangeur.start();
	    while( groupeOfPhilos.activeCount()!=0) Thread.yield();	 // remplacer par une attente passive de type join()
	    long dateFin = System.currentTimeMillis();
	    double duree = (dateFin -  dateDepart) / 1000d;
	 
	    System.out.printf("Le repas est fini en %.3f s.", duree);
	}
}
