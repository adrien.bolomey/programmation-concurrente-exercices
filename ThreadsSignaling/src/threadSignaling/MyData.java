package threadSignaling;

import java.util.Random;

public class MyData {
    private final int UPPERBOUND = 1001;
    private int randomNum;

    public synchronized int getMyData() {
        return randomNum;
    }

    public synchronized void setMyData() {
        randomNum = new Random().nextInt(UPPERBOUND);

    }
}
