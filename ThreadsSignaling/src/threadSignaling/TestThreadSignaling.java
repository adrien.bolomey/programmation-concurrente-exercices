package threadSignaling;

public class TestThreadSignaling {
    private MySignal mySignal = new MySignal();
    private MyData myData = new MyData();
    private MyWaitNotify myWaitNotify = new MyWaitNotify();


    public TestThreadSignaling(){
        new Thread(new Producer()).start();
        new Thread(new Consumer()).start();
    }

    public static void main (String[] args){
        new TestThreadSignaling();
    }

    public class Producer implements Runnable{
        @Override
        public void run() {
            myWaitNotify.doNotify();
            long end = System.currentTimeMillis()+5000;
            while (System.currentTimeMillis() < end){
                myData.setMyData();
            }
            mySignal.setHasDataToProcess(false);
            System.out.println("Thread Producer - " +Thread.currentThread().getId()+" terminated");

        }
    }

    public class Consumer implements Runnable{
        @Override
        public void run() {
            myWaitNotify.doWait();
            System.out.println("Thread Consumer - " +Thread.currentThread().getId()+" terminated");
        }
    }


}
