package threadSignaling_bis;

public class WaitingThread extends Thread {

    private String threadName = "No Name";
    private MySyncClass mySync = null;
    private MyDataClass myData = null;

    public WaitingThread(String threadName, MySyncClass mySync, MyDataClass myData) {
        this.threadName = threadName;
        this.mySync = mySync;
        this.myData = myData;
    }

    public void run(){
        System.out.println(threadName + " started ...");
        mySync.doWait();
        System.out.println(threadName + " received " + myData.getData());
        System.out.println(threadName + " ended !!");
    }
}