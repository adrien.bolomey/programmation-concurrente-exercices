package threadSignaling_bis;

public class TestThreadSignaling {

    public static void main(String[] args) {

        MySyncClass mySync = new MySyncClass();
        MyDataClass myData = new MyDataClass();

        new WaitingThread("Waiting Thread B", mySync, myData).start();
        new WaitingThread("Waiting Thread C", mySync, myData).start();
        new WaitingThread("Waiting Thread D", mySync, myData).start();
        new CallingThread("Calling Thread A", mySync, myData).start();
    }

}
