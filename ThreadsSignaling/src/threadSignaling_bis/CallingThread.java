package threadSignaling_bis;

public class CallingThread extends Thread {

    private final int FIRSTDATA = 1;
    private final int SECONDTDATA = 2;

    private String threadName = "No Name";
    private MySyncClass mySync = null;
    private MyDataClass myData = null;

    public CallingThread(String threadName, MySyncClass mySync, MyDataClass myData) {
        this.threadName = threadName;
        this.mySync = mySync;
        this.myData = myData;
    }

    public void run(){
        System.out.println(threadName + " started ...");
        myData.setData(FIRSTDATA);
        System.out.println(threadName + " notifying");
        mySync.doNotify();
        try {
            Thread.sleep(2000L);					// wait to ensure first waiting thread completed
        } catch (InterruptedException e) {
            System.out.println("Sleep Interrupted Exception");
            e.printStackTrace();
        }
        myData.setData(SECONDTDATA);
        System.out.println(threadName + " notifying All");
        mySync.doNotifyAll();
        try {
            Thread.sleep(2000L);					// wait to ensure last waiting threads completed
        } catch (InterruptedException e) {
            System.out.println("Sleep Interrupted Exception");
            e.printStackTrace();
        }
        System.out.println(threadName + " ended !!");
    }
}