package threadSignaling_bis;

public class MySyncClass {

    Monitor myMonitor = new Monitor();
    boolean isSignalled = false;

    public void doWait(){				// wait
        synchronized(myMonitor){
            try {
                myMonitor.wait();
            } catch(InterruptedException e) {
                System.out.println("Monitor wait exception");
                e.printStackTrace();
            }
        }
    }

    public void doNotify(){				// notify
        synchronized(myMonitor){
            myMonitor.notify();
        }
    }

    public void doNotifyAll(){			// notifyAll (the rest)
        synchronized(myMonitor){
            myMonitor.notifyAll();
        }
    }

}