package threadSignaling_bis;

public class MyDataClass {

    private int data = 0;

    public synchronized void setData(int data) {
        this.data = data;
    }

    public synchronized int getData() {
        return this.data;
    }
}
