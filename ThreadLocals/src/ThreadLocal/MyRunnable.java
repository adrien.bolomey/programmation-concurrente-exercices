package ThreadLocal;

public class MyRunnable implements Runnable{
    private ThreadLocal<String> threadLocal = new ThreadLocal<String>();
    private Integer runCounter = 0;

    @Override
    public void run() {
        threadLocal.set("Thread "+ Thread.currentThread().getId());
        runCounter++;
        System.out.println(Thread.currentThread().getName()+ " internal count: " + runCounter + " and is " + threadLocal.get());

        try{
            Thread.sleep(2000);
        } catch (InterruptedException e){
            //catch somewhere
        }
    }
}
