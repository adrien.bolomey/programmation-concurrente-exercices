package exc.myFirstRunnable;

public class TestMyRunnable{

    private static class MyRunnable implements Runnable {

        @Override
        public void run() {
            try {
                for (int i = 0; i < 5; i++) {
                    Thread.sleep((long) (Math.random() * 5000));
                    System.out.println(Thread.currentThread().getName() + " stop " + i);
                }
            } catch (InterruptedException e) {
                System.out.println("Runnable InterruptedException");
            }
            System.out.println(Thread.currentThread().getName() + " finished");
        }
    }

    public static void main(String[] args){
        final int NBR_OF_THREADS = 10;
        Thread[] thread = new Thread[NBR_OF_THREADS];

        for (int i=0; i<NBR_OF_THREADS; i++){
            thread[i] = new Thread(new MyRunnable());
            thread[i].start();
        }

        try {
            for (int i=0; i<NBR_OF_THREADS; i++){
                thread[i].join();
            }
        } catch (InterruptedException e){
            System.out.println("Join InterruptedException");
        }
        System.out.println(Thread.currentThread().getName() + " finished");
    }
}

