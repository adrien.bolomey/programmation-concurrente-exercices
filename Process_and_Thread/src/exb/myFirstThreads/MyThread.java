package exb.myFirstThreads;

public class MyThread extends Thread{
    private final int threadNumber;

    public MyThread(int threadNumber){
        this.threadNumber = threadNumber;
        System.out.println(this.threadNumber + " - " + getName() + " created");
    }

    public void run(){
        System.out.println(this.threadNumber + " - " + getName() + " running");
        long delay = (long)(Math.random()*10);
        System.out.println(this.threadNumber + " - " + getName() + " sleeping " + "for " + delay + "s");

        try {
            Thread.sleep(delay*1000L);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        System.out.println(this.threadNumber + " - " + getName() + " ending");
    }
}
