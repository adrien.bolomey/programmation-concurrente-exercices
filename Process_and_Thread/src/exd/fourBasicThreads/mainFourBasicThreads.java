package exd.fourBasicThreads;

import java.util.*;

// four different manner to launch and execute threads ...
public class mainFourBasicThreads {

    public static void main(String[] args) {	// starting from here

        (new Thread(new Worker1())).start();	// start worker 1 as runnable class

        new Worker2();							// start worker 2 as inner class thread

        (new Thread(new Runnable() {			// start worker 3 as Runnable
            public void run() {
                makeWork.work(2, 5);
            }
        })).start();

        (new Thread(() -> makeWork.work(2, 5))).start();	// start worker 4 as Thread

        makeWork.work(5, 10);	// main thread work also !!
    }
}

// inner class implementing Runnable
class Worker1 implements Runnable {
    public void run() {
        makeWork.work(2, 5);
    }
}

// inner class extending Thread
class Worker2 extends Thread {
    public Worker2() {
        start();	// thread stated within object constructor
    }

    public void run() {
        makeWork.work(2, 5);
    }
}

// class/object to simulate something to do that take computing time, note that all static, no instance required !
class makeWork {
    private static Random rand = new Random();

    // working
    public static void work(int a, int b) { 	// working for a random interval of time between a(min) and b(max)
        print(" >>> started");
        for (int i = 0, n = rand.nextInt(b - a) + a; i < n; ++i) {
            print(" working ...");
            calculate();
        }
        print(" terminated !!!");
    }

    // display progress
    private static void print(String text) {
        System.out.println(Thread.currentThread().getName() + text);
    }

    // take time to calculate ...
    private static void calculate() {
        double y;	// ok not used, we know !
        for (int i = 0; i < 1000000L; ++i)
            y = Math.cos(Math.sqrt(rand.nextDouble()));
    }
}