package pipes;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class proba {
    public static void main(String[] args) throws IOException{

        final PipedOutputStream outputThread1 = new PipedOutputStream();
        final PipedInputStream inputThread1 = new PipedInputStream(outputThread1);

        final PipedOutputStream outputThread2 = new PipedOutputStream();
        final PipedInputStream inputThread2 = new PipedInputStream(outputThread2);


        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    System.out.println("1st msg:");
                    outputThread1.write("Hello there to Thread2!".getBytes());
                    outputThread1.flush();
                    System.out.println("TH1 received");
                    int data = inputThread2.read();
                    while (data != -1){
                        System.out.print ((char) data);
                        data = inputThread2.read();
                    }
                    inputThread2.close();
                    System.out.println("TH1 finished");
                }catch (IOException e){
                    System.out.println("TH1 error");
                    e.printStackTrace();
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    System.out.println("TH2 received");
                    int data = inputThread1.read();
                    while (data != -1){
                        System.out.print((char) data);
                        data = inputThread1.read();
                    }
                    inputThread1.close();

                    System.out.println("2nd msg:");
                    outputThread2.write("Hello there to Thread1!".getBytes());
                    outputThread2.flush();

                    System.out.println("TH2 finished");
                }catch (IOException e){
                    System.out.println("TH2 error");
                    e.printStackTrace();
                }
            }
        });

        thread1.start();
        thread2.start();
    }
}
