package pipes;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class RingThread implements Runnable {

    private PipedOutputStream prev;
    private PipedInputStream next;

    public RingThread(PipedOutputStream prev, PipedInputStream next) throws IOException {
        this.prev = prev;
        this.next = next;
    }


    @Override
    public void run() {

    }
}
