package exd;

/*
- le remplacement de StringBuilder par StringBuffer, était-ce bien nécessaire ?
Oui, cela permet d'avoir un système de Stream

- les "synchonized" dans la classe SafeShare
Oui, cela permet de synchroniser les accès au Thread

- volatile: permet que la valeur soit à jour dans la mémoire RAM


- atomique: noyau qu'on ne peut pas casser
-> une instruction non atomique peuvent être cassée pour laisser place à une autre instruction




 */
public class SafeShare {

    volatile private int globalCounter = 0;					// count all the global calls of method add()

    private Integer globalCounterLock = new Integer(1);		// let synchronize globalCounter

    StringBuffer buffer = new StringBuffer("buffered "); 	// StringBuffer is thread-safe and replace StringBuilder
    // to store the shared string

    // extend the string
    public void add(String text){
        this.buffer.append(text); 		// self synchronized
        synchronized(this.globalCounterLock) {
            this.globalCounter++;
        }
    }

    // return the string
    public String read(){
        return this.buffer.toString();	// self synchronized

    }

    // return the number of add
    public int getCount() {
        synchronized(this.globalCounterLock) {	// not mandatory !
            return this.globalCounter;
        }
    }

}