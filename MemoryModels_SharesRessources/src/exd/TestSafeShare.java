package exd;
/*
- les variables sharedInstance et threadslock en protected et partagés aux instances de l'inner class MyRunnable, pourquoi cela est possible ici ? Est-ce propre ?
Le synchronized est réalisé sur l'objet SafeShare. Pour threadslock, il s'agit de la même chose. Oui.

- les "synchonized" dans la méthode run() ?
Ils permettent l'incrément correct de la variable locale "localCount".
Ils empêchent l'interruption par d'autres Threads.


- les "yield" dans la méthode run() ?
Permet de forcer le reschulding -> l
 */


public class TestSafeShare {

    protected SafeShare sharedInstance = new SafeShare();	// shared object global with all threads - why ?
    protected Integer threadsLock = new Integer(1);			// global also - let synchronize threads - why ?

    // constructor that create runnable threads
    public TestSafeShare() {
        new Thread(new MyRunnable()).start();
        new Thread(new MyRunnable()).start();
        new Thread(new MyRunnable()).start();
    }

    // starting point
    public static void main(String[] args) {
        new TestSafeShare();	// use constructor to launch threads
        System.out.println(Thread.currentThread().getName() + " - End");
    }

    // each thread call methods of the shared object
    public class MyRunnable implements Runnable{

        // private SafeShare instance = null;
        private int localCount = 0;

        public void run(){

            synchronized(threadsLock) {		// please explain why this should be useful ? (remove to test effect)
                sharedInstance.add("with first " + Thread.currentThread().getName() + " ");
                localCount++;
            }

            Thread.yield();					// not mandatory, force rescheduling

            synchronized(threadsLock) {		// please explain why this should be useful ? (remove to test effect)
                sharedInstance.add("with second " + Thread.currentThread().getName() + " ");
                localCount++;
            }

            Thread.yield();					// not mandatory, force rescheduling

            synchronized(threadsLock) {		// please explain why this should be useful ? (remove to test effect)
                System.out.println(Thread.currentThread().getName() + " - terminated with: " + sharedInstance.read());
                System.out.println(Thread.currentThread().getName() + " - terminated after " + this.localCount + " local add and " +
                        sharedInstance.getCount() + " global add.");
            }
        }
    }

}