package exa_b;

public class NotSafeShare {
    private int counter = 0;
    StringBuilder builder = new StringBuilder("Builded ");

    public synchronized void add(String text){
        this.builder.append(text);
        counter++;
        System.out.println(Thread.currentThread().getName() + " global count: " + counter);
    }

    public String read(){
        return this.builder.toString();
    }

}
