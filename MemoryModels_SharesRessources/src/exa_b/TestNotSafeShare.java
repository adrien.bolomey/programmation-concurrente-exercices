/*
On voit clairement dans ce code, la partie de races conditions que l'on a étudié et notamment les objets partagés
Les 3 threads ajoute des texte sans sans vraiment avoir de priorité sur qui passe en premier, etc...

 */


package exa_b;

public class TestNotSafeShare {

    NotSafeShare sharedInstance = new NotSafeShare();

    public TestNotSafeShare() {
        new Thread(new MyRunnable(sharedInstance)).start();
        new Thread(new MyRunnable(sharedInstance)).start();
        new Thread(new MyRunnable(sharedInstance)).start();
    }

    public static void main(String[] args) {
        new TestNotSafeShare();
        System.out.println(Thread.currentThread().getName() + " - End");
    }

    public class MyRunnable implements Runnable{
        int count = 0;
        NotSafeShare instance = null;


        public MyRunnable(NotSafeShare instance){
            this.instance = instance;
        }

        public void run(){
            count++;
            System.out.println(Thread.currentThread().getName() + " local count: " + count);
            this.instance.add("with first ");
            count++;
            System.out.println(Thread.currentThread().getName() + " local count: " + count);
            this.instance.add("and second text ");
            count++;
            System.out.println(Thread.currentThread().getName() + " local count: " + count);
            System.out.println(Thread.currentThread().getName() + " - " + this.instance.read());
        }
    }

}